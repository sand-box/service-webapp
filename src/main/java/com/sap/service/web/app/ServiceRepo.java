package com.sap.service.web.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.service.model.Service;

public final class ServiceRepo {

	private static final ServiceRepo INSTANCE = new ServiceRepo(); 
	private Map<Long, Service> services;
	private int count;

	private ServiceRepo() {
		if (services == null) {
			services = new HashMap<Long, Service>();
			count = 0;
		}
		Service service = new Service("name", "description", "note", 0.0, 0.0, 0.0, 0.0);
		create(service);
	}
	
	public Long create(Service service) {
		if (service.getId() == null) {
			service.setId((long) count);
			services.put((long) count, service);
			return (long) count++;
		}
		else return -1L;
	}
	
	public Service read(Long id) {
		if (services.containsKey(id)) {
			return services.get(id);
		} else return null;
	}
	
	public List<Service> readAll() {
		List<Service> serviceList = new ArrayList<Service>(services.values());
		return serviceList;
	}
	
	public Long update(Service service) {
		Long id = service.getId();
		if (services.containsKey(id)) {
			services.put(id, service);
			return id;
		} else return -1L;
	}
	
	public Service delete(Long id) {
		if (services.containsKey(id)) {
			Service service = services.get(id);
			services.remove(id);
			return service;
		} else return null;
	}
	
	public static final ServiceRepo getInstance() {
		return INSTANCE;
	}
}
