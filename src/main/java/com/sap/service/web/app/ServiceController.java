package com.sap.service.web.app;


import java.util.List;

import com.sap.service.contract.ServiceContract;
import com.sap.service.model.Service;


public class ServiceController implements ServiceContract{
	
	public Long create(Service service) {
		return ServiceRepo.getInstance().create(service);
	}
	
	public Service read(long id) {
		return ServiceRepo.getInstance().read(id);
	}
	
	public List<Service> readAll() {
		return ServiceRepo.getInstance().readAll();
	}

	public Long update(Service service) {
		return ServiceRepo.getInstance().update(service);
	}

	public Service delete(long id) {
		return ServiceRepo.getInstance().delete(id);
	}

}
